/**
 * 
 */
package com.polymorphism;

/**
 * @author ACER
 *
 */
public class CompileTimeDemo {

	public int sum(int a, int b) {
		return a + b;
	}

	public int sum(int a, int b, int c) {
		return a + b + c;
	}

	public float sum(float a, int b, int c) {
		return a + b + c;
	}

}
