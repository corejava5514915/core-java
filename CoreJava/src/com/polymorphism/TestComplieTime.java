/**
 * 
 */
package com.polymorphism;

/**
 * @author ACER
 *
 */
public class TestComplieTime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CompileTimeDemo compileTimeDemo = new CompileTimeDemo();
		int resu1 = compileTimeDemo.sum(10, 20);
		System.out.println("result is:" + resu1);
		int resu2 = compileTimeDemo.sum(10, 20, 50);
		System.out.println("result is:" + resu1);
		float result3 = compileTimeDemo.sum(2f, 4, 6);
		System.out.println("result is:" + result3);

	}

}
