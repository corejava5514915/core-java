/**
 * 
 */
package com.polymorphism;

/**
 * @author ACER
 *
 */
public class RuntimeDemoToOverride extends RunTimeDemo {

	@Override
	public String display(String msg) {
		System.out.println("child has it's own business");
		return "child : " + msg;
	}

}
