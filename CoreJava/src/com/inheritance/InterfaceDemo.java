/**
 * 
 */
package com.inheritance;

/**
 * @author ACER
 *
 */
public class InterfaceDemo implements InterfaceOne, InterfaceTwo {
	@Override
	public void test() {
		System.out.println("this is from InterfaceOne");
	}

	@Override
	public void print(String msg) {
		System.out.println("this is from Interfacetwo:" + msg);
	}

}
