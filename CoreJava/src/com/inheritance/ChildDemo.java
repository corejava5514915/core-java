/**
 * 
 */
package com.inheritance;

/**
 * @author ACER
 *
 */
public class ChildDemo extends SuperDemo {

	/*
	 * @Override public void test() { System.out.println("this is child method"); }
	 */
	@Override
	public void print(int a, int b) {
		System.out.println("child class can impproove it's own logic");
		super.print(a, b);
		System.out.println("this is in child method from child!!!" + (a + b));

	}

}
