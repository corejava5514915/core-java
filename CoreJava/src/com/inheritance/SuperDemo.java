/**
 * 
 */
package com.inheritance;

/**
 * @author ACER
 *
 */
public class SuperDemo {

	public void test() {
		System.out.println("this is parent method");
	}

	public void print(int a, int b) {
		System.out.println("this is in print method from parent!!!" + (a + b));

	}

}
